﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployee(EmployeeRequest createEmployeeRequest)
        {
            if (!await CheckRolesAsync(createEmployeeRequest.Roles))
                return BadRequest();

            var employee = await createEmployeeRequest.ConvertToEmployee();
            employee.Roles = (await Task.WhenAll(createEmployeeRequest.Roles.Select(async x => await _rolesRepository.GetByIdAsync(x)))).ToList();
           

            await _employeeRepository.CreateAsync(employee);
            return employee.Id;
        }
        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee(Guid employeeId)
        {
            var removed = await _employeeRepository.RemoveAsync(employeeId);
            if (!removed)
                return NotFound();

            return Ok();
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee(Guid id, EmployeeRequest employeeRequest)
        {
            if (!await CheckRolesAsync(employeeRequest.Roles))
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee is null)
                return NotFound();

            employee.Email = employeeRequest.Email;
            employee.FirstName = employeeRequest.FirstName;
            employee.LastName = employeeRequest.LastName;
            employee.AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount;
            employee.Roles = (await Task.WhenAll(employeeRequest.Roles.Select(async x => await _rolesRepository.GetByIdAsync(x)))).ToList();
            return Ok();
        }

        #region static Methods
        private async Task<bool> CheckRolesAsync(IEnumerable<Guid> roleIds)
        {
            var tasks = roleIds.Select(x => _rolesRepository.ExistsAsync(x));
            var results = await Task.WhenAll(tasks);
            return results.All(x => x);
        }
        #endregion
    }
}