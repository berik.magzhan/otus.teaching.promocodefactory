﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int AppliedPromocodesCount { get; set; }
        public IEnumerable<Guid> Roles { get; set; }


        #region static methods

        public async Task<Employee> ConvertToEmployee()
        {
            return new Employee()
            {
                Id = Guid.NewGuid(),
                Email = this.Email,
                FirstName = this.FirstName,
                LastName = this.LastName,
                AppliedPromocodesCount = this.AppliedPromocodesCount,

            };
        }

        #endregion
    }
}
